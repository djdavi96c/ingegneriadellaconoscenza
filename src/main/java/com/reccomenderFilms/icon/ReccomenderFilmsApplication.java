package com.reccomenderFilms.icon;

import javax.servlet.DispatcherType;

import com.reccomenderFilms.icon.mapper.CountryMapper;
import com.reccomenderFilms.icon.mapper.FilmMapper;
import com.reccomenderFilms.icon.mapper.GenreMapper;
import com.reccomenderFilms.icon.reccomender.RecommenderSystem;
import com.reccomenderFilms.icon.reccomender.impl.RecommenderImpl;
import com.reccomenderFilms.icon.structure.Film;
import com.reccomenderFilms.icon.util.TaskRetrieveInformationIMBD;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cache.RedisCacheManagerBuilderCustomizer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


@ComponentScan("com.reccomenderFilms.icon")
@SpringBootApplication
@EnableCaching
@EnableScheduling
public class ReccomenderFilmsApplication {

	private static final Logger logger = LoggerFactory.getLogger(ReccomenderFilmsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ReccomenderFilmsApplication.class, args);
	}
	@Value("${openimbd.api.key}")
	private String openImbdApi;
	@Value("${openimbd.api.patternSearch:i}")
	private String patternSearch;

	@Autowired
	private CountryMapper countryMapper;
	@Autowired
	private FilmMapper filmMapper;
	@Autowired
	private GenreMapper genreMapper;
	@Autowired
	private HikariDataSource hikariDataSource;
	@Bean
	public FilterRegistrationBean FileUploadFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new org.primefaces.webapp.filter.FileUploadFilter());
		registration.setName("PrimeFaces FileUpload Filter");
		registration.setDispatcherTypes(DispatcherType.FORWARD);
		return registration;
	}

	@Bean
	public RedisCacheConfiguration cacheConfiguration() {
		return RedisCacheConfiguration.defaultCacheConfig()
				.entryTtl(Duration.ofMinutes(60))
				.disableCachingNullValues()
				.serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer()));
	}
	@Bean
	public RedisCacheManagerBuilderCustomizer redisCacheManagerBuilderCustomizer() {
		return (builder) -> builder
				.withCacheConfiguration("film-cache",
						RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofMinutes(60)));
	}

	@Scheduled(fixedDelay = 10000)
	public void insertCsvInformation() {
		List<String> films = new ArrayList<>();
		LineIterator it = null;
		try {
			it = FileUtils.lineIterator(new File("caricamento.csv"), "UTF-8");
		it.nextLine();
		while (it.hasNext()) {
			String line = it.nextLine();
			films.add(line.split(",")[1]);
		}
		it.close();
		ExecutorService executor = Executors.newFixedThreadPool(10);
		int divider=films.size()/100 > 0 ? 100: 50;
		int numberOfTask = films.size()/divider;
		Integer counter = 0;
		List<Future<String>> list = new ArrayList<Future<String>>();
		for(int j=0; j<numberOfTask; j++) {
			Callable<String> callable = new TaskRetrieveInformationIMBD(new RestTemplateBuilder().build(),films.subList(j*divider, (j+1) * divider),filmMapper,countryMapper,genreMapper,openImbdApi,patternSearch);
			Future<String> future = executor.submit(callable);
		}
		if(films.size()!=(numberOfTask*divider)) {
			Callable<String> callable = new TaskRetrieveInformationIMBD(new RestTemplateBuilder().build(),films.subList(numberOfTask*divider,(numberOfTask*divider)+(films.size()-(numberOfTask*divider))),filmMapper,countryMapper,genreMapper,openImbdApi,patternSearch);
			Future<String> future = executor.submit(callable);
		}
		for(Future<String> fut : list){
			try {
				String getElement = fut.get();
				counter+=Integer.valueOf(getElement);
				logger.info("Elaborati n:"+counter);
			} catch (InterruptedException | ExecutionException e) {
				logger.error("Errore esecuzione thread",e);
			}
		}
		executor.shutdown();
		logger.info("Ho terminato il caricamento delle informazioni aggiuntive");
		} catch (IOException e) {
			logger.error("File caricamento.csv non presente, quindi non è previsto un caricamento");
		}
	}


//	@Bean
//	public void insertRating() throws IOException {
//		Stream<String> stream = Files.lines(Paths.get("C:\\Users\\dcaldarulo\\Desktop\\ingegneriadellaconoscenza\\ratings.csv"));
//		List<String>elements = 	stream.collect(Collectors.toList());
//		ExecutorService executor = Executors.newFixedThreadPool(10);
//		int divider=elements.size()/100 > 0 ? 100: 50;
//		int numberOfTask = elements.size()/divider;
//		List<Future<String>> list = new ArrayList<Future<String>>();
//		for(int j=0; j<numberOfTask; j++) {
//			Callable<String> callable = new TaskInsertVotesToDb(elements.subList(j*divider, (j+1) * divider),ratingMapper);
//			Future<String> future = executor.submit(callable);
//		}
//		if(elements.size()!=(numberOfTask*divider)) {
//			Callable<String> callable = new TaskInsertVotesToDb(elements.subList(numberOfTask*divider,(numberOfTask*divider)+(elements.size()-(numberOfTask*divider))),ratingMapper);
//			Future<String> future = executor.submit(callable);
//		}
//		for(Future<String> fut : list){
//			try {
//
//				String getElement = fut.get();
//			} catch (InterruptedException | ExecutionException e) {
//				logger.error("Errore esecuzione thread",e);
//			}
//		}
//		executor.shutdown();
//		logger.info("Ho finito di importare i rating");
//	}


}
