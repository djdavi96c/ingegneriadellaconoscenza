package com.reccomenderFilms.icon.structure;

import lombok.Data;

import java.io.Serializable;

@Data
public class Genre implements Serializable {
    private Long id;
    private String description;
}
