package com.reccomenderFilms.icon.structure;

import lombok.Data;

import java.util.List;
@Data
public class Question {
    private Long id;
    private String question;
    private List<Answer> answers;

}
