package com.reccomenderFilms.icon.structure;

import lombok.Data;

@Data
public class Questionario {
    public Question question;
    public Answer answer;

    public Questionario(Question question, Answer answer) {
        this.question = question;
        this.answer = answer;
    }
}
