package com.reccomenderFilms.icon.structure;

import lombok.Data;

@Data
public class Answer {
    private Long id;
    private String answer;
    private Long nextQuestionId;
    private Long questionId;
}
