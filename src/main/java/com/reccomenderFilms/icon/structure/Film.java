package com.reccomenderFilms.icon.structure;



import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class Film implements Serializable {
    private Long id;
    private String imbdId;
    private String name;
    private Boolean isRestricted;
    private Boolean winOscar;
    private BigDecimal imdbRating;
    private Date released;
    private Integer runtime;
    private Boolean isFilmReflectRealLife;
    private List<Genre> genreeList;
    private List<Country> countryList;
    private Integer userVotation;
    private String linkImage;


}
