package com.reccomenderFilms.icon.mapper;

import com.reccomenderFilms.icon.structure.Answer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AnswerMapper {

    @Select("Select a.id as id,a.ANSWER as answer,aq.EXT_NEXT_QUESTIONS as nextQuestionId,aq.EXT_QUESTION as questionId from ANSWER_QUESTION aq join answer a on aq.EXT_ANSWER=a.id where aq.EXT_QUESTION=${questionId} ")
    public List<Answer> retrieveDetail(@Param("questionId") Long questionId);
}
