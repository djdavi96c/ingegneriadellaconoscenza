package com.reccomenderFilms.icon.mapper;

import com.reccomenderFilms.icon.configuration.RedisCache;
import com.reccomenderFilms.icon.structure.Country;
import org.apache.ibatis.annotations.*;

import java.util.List;

@CacheNamespace(implementation = RedisCache.class)
@Mapper
public interface CountryMapper {

    @Insert("Insert into country (name) values ('${description}')")
    @Options(useGeneratedKeys=true, keyProperty = "country.id", keyColumn = "id")
    Long insertCountry(Country country, @Param("description")String description);

    @Select("Select id from country where name like '%${description}%' limit 1")
    Long retrieveCountry(@Param("description")String description);

    @Insert("Insert into country_film(ext_film,ext_country) values (${ext_film},${ext_country})")
    void insertCountryFilmCorrelation(@Param("ext_film")Long idFilm,@Param("ext_country")Long idCountry);


    @Select("Select cy.name from country_film cf join film fm on cf.ext_film=fm.id join country cy on cf.ext_country=cy.id where cf.ext_film=${id}")
    @Options(useCache = true)
    List<Country> retrieveCountriesByIdFilm(@Param("id")Long idFilm);


}
