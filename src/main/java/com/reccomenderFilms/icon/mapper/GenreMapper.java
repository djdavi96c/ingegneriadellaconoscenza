package com.reccomenderFilms.icon.mapper;

import com.reccomenderFilms.icon.configuration.RedisCache;
import com.reccomenderFilms.icon.structure.Genre;
import org.apache.ibatis.annotations.*;

import java.util.List;

@CacheNamespace(implementation = RedisCache.class)
@Mapper
public interface GenreMapper {
    @Select("Select id from genre where description like '%${genreDescription}%' limit 1")
    Long retrieveGenre(@Param("genreDescription") String genreDescription);


    @Select("Select gn.* from film_genre fg join genre gn on fg.ext_genre=gn.id join film fm on fg.ext_film=fm.id where fg.ext_film=${id}")
    @Options(useCache = true)
    List<Genre> retrieveGenreeByIdFilm(@Param("id")Long idFilm);

    @Insert("insert into film_genre(ext_genre,ext_film) values(${ext_genre},${ext_film})")
    Long insertFilmGenreCorrelation(@Param("ext_genre")Long idGenree,@Param("ext_film")Long idFilm);
}
