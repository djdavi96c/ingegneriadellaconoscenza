package com.reccomenderFilms.icon.mapper;

import com.reccomenderFilms.icon.configuration.RedisCache;
import com.reccomenderFilms.icon.structure.Film;
import org.apache.ibatis.annotations.*;

import java.sql.Date;
import java.util.List;
@CacheNamespace(implementation = RedisCache.class)
@Mapper
public interface FilmMapper {


    @Select("select  max(id) from film ")
    long getLastIdFilm();

    @Options(useCache = true)
    @Select("Select * from film")
    List<Film> getAllFilms();

    @Insert("Insert into film_genre(ext_genre,ext_film) values(${ext_genre},${ext_film})")
    long insertFilmGenreCorrelation(@Param("ext_genre")Long idGenre,@Param("ext_film")Long idFilm);

    @Select("Select * from film")
    List<Film> retrieveAllFilms();

    @Update("Update film SET isRestricted = ${isRestricted}, winOscar = ${winOscar}, imdbRating =${imdbRating}, released = '${released}', runtime =${runtime}, isFilmReflectRealLife =${isFilmReflectRealLife} WHERE id = ${id}")
    long updateFilmTableWithDate(@Param("id") Long id, @Param("isRestricted")Boolean restriction, @Param("winOscar")Boolean winOscar, @Param("imdbRating")Double rating, @Param("released") Date release, @Param("runtime") Integer duration,@Param("isFilmReflectRealLife") Boolean isFilmReflectRealLife);

    @Insert("insert into film(imbdId,name,isRestricted,winOscar,imdbRating,released,runtime,isFilmReflectRealLife) values('${idImbd}','${name}',${isRestricted},${winOscar},${imdbRating},'${released}',${runtime},${isFilmReflectRealLife})")
    @Options(useGeneratedKeys=true, keyProperty = "film.id", keyColumn = "id")
    void insertFilm(@Param("idImbd")String idImbd,@Param("name") String name,@Param("isRestricted")Boolean restriction, @Param("winOscar")Boolean winOscar, @Param("imdbRating")Double rating, @Param("released") Date release, @Param("runtime") Integer duration,@Param("isFilmReflectRealLife") Boolean isFilmReflectRealLife,Film film);

    @Update("Update film SET isRestricted = ${isRestricted}, winOscar = ${winOscar}, imdbRating =${imdbRating}, runtime =${runtime}, isFilmReflectRealLife =${isFilmReflectRealLife} WHERE id = ${id}")
    long updateFilmTable(@Param("id") Long id, @Param("isRestricted")Boolean restriction, @Param("winOscar")Boolean winOscar, @Param("imdbRating")Double rating, @Param("runtime") Integer duration,@Param("isFilmReflectRealLife") Boolean isFilmReflectRealLife);

    @Select("Select * from film where id=${id}")
    Film retrieveFilmById(@Param("id") Long idFilm);

}
