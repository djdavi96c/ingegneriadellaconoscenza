package com.reccomenderFilms.icon.mapper;

import com.reccomenderFilms.icon.structure.Question;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


@Mapper
public interface QuestionMapper {

    @Select("select * from questions")
    public List<Question> getQuestions();

    @Select("select * from questions where id=1")
    public Question getMainQuestion();
}
