package com.reccomenderFilms.icon.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

@Mapper
public interface RatingMapper {
    @Insert("Insert into USER_RATING values (${userID}, ${ext_id_film}, ${rating})")
    long insertRating(@Param("userID")Integer userID, @Param("ext_id_film")Integer ext_id_film, @Param("rating")BigDecimal rating);
}
