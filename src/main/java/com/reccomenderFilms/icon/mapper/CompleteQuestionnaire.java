package com.reccomenderFilms.icon.mapper;

import com.reccomenderFilms.icon.structure.CompletedQuesionnaire;
import org.apache.ibatis.annotations.*;

@Mapper
public interface CompleteQuestionnaire {

    @Insert("insert into COMPLETED_QUESTIONNAIRE (id) values (null)")
    @Options(useGeneratedKeys=true, keyProperty = "id", keyColumn = "id")
    long createCompleteQuestionnaire(CompletedQuesionnaire completeQuestionnaire);

    @Insert("insert into QUESTIONNAIRE_INFORMATION (EXT_COMPLETED_QUESTIONNAIRE,EXT_QUESTION,EXT_ANSWER) values (${idCompleteQuestionnaire},${idQuestion},${idAnswer})")
    void insertInformationCompleteQuestionnaire(@Param("idCompleteQuestionnaire") Long idCompleteQuestionnaire,@Param("idQuestion") Long idQuestion,@Param("idAnswer") Long idAnswer);

}
