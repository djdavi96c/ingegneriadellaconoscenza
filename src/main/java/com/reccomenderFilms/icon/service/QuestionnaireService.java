package com.reccomenderFilms.icon.service;

import com.reccomenderFilms.icon.structure.Questionario;

import java.util.List;

public interface QuestionnaireService {

    public void saveQuestionarioInformation(List<Questionario> questionarioList);
}
