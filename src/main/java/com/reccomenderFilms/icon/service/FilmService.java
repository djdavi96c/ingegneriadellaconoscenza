package com.reccomenderFilms.icon.service;

import com.reccomenderFilms.icon.structure.Film;

import java.util.Date;
import java.util.List;

public interface FilmService {
    public List<Film> getAllFilms();
    public Film getFilmById(Long id);
}
