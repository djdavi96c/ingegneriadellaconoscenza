package com.reccomenderFilms.icon.service.impl;

import com.reccomenderFilms.icon.mapper.CompleteQuestionnaire;
import com.reccomenderFilms.icon.service.QuestionnaireService;
import com.reccomenderFilms.icon.structure.CompletedQuesionnaire;
import com.reccomenderFilms.icon.structure.Questionario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionnaireImpl implements QuestionnaireService {
    @Autowired
    private CompleteQuestionnaire completeQuestionnaire;

    @Override
    public void saveQuestionarioInformation(List<Questionario> questionarioList) {
        CompletedQuesionnaire completedQuestionnaire = new CompletedQuesionnaire();
        Long idQuestionnaireCreate = completeQuestionnaire.createCompleteQuestionnaire(completedQuestionnaire);
        questionarioList.forEach(questionario -> completeQuestionnaire.insertInformationCompleteQuestionnaire(completedQuestionnaire.getId(),questionario.getQuestion().getId(),questionario.getAnswer().getId()));
    }
}
