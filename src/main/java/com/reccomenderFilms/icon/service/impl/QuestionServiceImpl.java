package com.reccomenderFilms.icon.service.impl;

import com.reccomenderFilms.icon.mapper.AnswerMapper;
import com.reccomenderFilms.icon.mapper.QuestionMapper;
import com.reccomenderFilms.icon.service.QuestionService;
import com.reccomenderFilms.icon.structure.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    private QuestionMapper questionMapper;
    @Autowired
    private AnswerMapper answerMapper;


    @Override
    public Question retrieveFirstQuestion() {
        Question question =  questionMapper.getMainQuestion();
        if(question!=null){
            question.setAnswers(answerMapper.retrieveDetail(question.getId()));
        }
        return  question;
    }

    @Override
    public List<Question> getAllQuestion() {
        List<Question> questionList =  questionMapper.getQuestions();
        if(questionList!=null && !questionList.isEmpty()){
            questionList.forEach(question -> question.setAnswers(answerMapper.retrieveDetail(question.getId())));
        }
        return questionList;
    }
}
