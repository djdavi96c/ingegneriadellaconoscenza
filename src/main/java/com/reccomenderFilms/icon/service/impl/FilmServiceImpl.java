package com.reccomenderFilms.icon.service.impl;

import com.reccomenderFilms.icon.mapper.CountryMapper;
import com.reccomenderFilms.icon.mapper.FilmMapper;
import com.reccomenderFilms.icon.mapper.GenreMapper;
import com.reccomenderFilms.icon.service.FilmService;
import com.reccomenderFilms.icon.structure.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.List;
@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmMapper filmMapper;
    @Autowired
    private CountryMapper countryMapper;

    @Autowired
    private GenreMapper genreMapper;

    public List<Film> getAllFilms() {
        List<Film> films = filmMapper.getAllFilms();
        films.forEach((p) -> {
            p.setGenreeList(genreMapper.retrieveGenreeByIdFilm(p.getId()));
            p.setCountryList(countryMapper.retrieveCountriesByIdFilm(p.getId()));
        });
        return films;
    }

    public Film getFilmById(Long id) {
        return filmMapper.retrieveFilmById(id);
    }

}


