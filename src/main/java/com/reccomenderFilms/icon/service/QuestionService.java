package com.reccomenderFilms.icon.service;

import com.reccomenderFilms.icon.structure.Question;

import java.util.List;

public interface QuestionService {

    public Question retrieveFirstQuestion();

    public List<Question> getAllQuestion();
}
