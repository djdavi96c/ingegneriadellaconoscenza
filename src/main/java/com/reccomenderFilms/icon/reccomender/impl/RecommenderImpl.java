package com.reccomenderFilms.icon.reccomender.impl;

import com.reccomenderFilms.icon.reccomender.RecommenderSystem;
import com.reccomenderFilms.icon.structure.Film;
import org.apache.commons.csv.writer.CSVWriter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

@Component
public class RecommenderImpl implements RecommenderSystem {
    private static final Logger logger = LoggerFactory.getLogger(RecommenderImpl.class);

//    public static void main(String args[]) {
//        DataModel dataModel;
//        UserSimilarity similarity;
//        UserNeighborhood neighborhood;
//        List<RecommendedItem> recommendations;
//
//        {
//            try {
//                dataModel = new FileDataModel(new ClassPathResource("rating.csv").getFile());
//                similarity = new PearsonCorrelationSimilarity(dataModel);
//                neighborhood = new NearestNUserNeighborhood(10, similarity, dataModel);
//                UserBasedRecommender recommender = new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
//                recommendations = recommender.recommend(2, 3);
//                for (RecommendedItem recommendation : recommendations) {
//                    System.out.println(recommendation);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (TasteException e) {
//                e.printStackTrace();
//            }
//
//
//        }
//    }

    @Override
    public Recommender rielaborateQuestionOrderByOldQuestionnaire() throws TasteException {
        UserBasedRecommender recommender = null;
        DataModel dataModel;
        UserSimilarity similarity;
        UserNeighborhood neighborhood;
        List<RecommendedItem> recommendations;
        try {
            dataModel = new FileDataModel(new File("ratings.csv"));

            similarity = new PearsonCorrelationSimilarity(dataModel);
            neighborhood = new NearestNUserNeighborhood(29, similarity, dataModel);
            recommender = new GenericUserBasedRecommender(dataModel, neighborhood, similarity);
        } catch (IOException e) {
            logger.error("Errore nel caricamento del file rating.csv");
        }
        return recommender;
    }


    public Long insertFilmVotationToCsv(List<Film> filmList,Long idUtente){
        String lastLine = "";
        FileWriter mFileWriter = null;
        try {
            if(idUtente==null) {

                LineIterator it = FileUtils.lineIterator(new File("ratings.csv"), "UTF-8");
                while (it.hasNext()) {
                    lastLine = it.nextLine();
                }
                idUtente = (Long.valueOf(lastLine.split(",")[0]) + 1);
            }
            mFileWriter = new FileWriter(new File("ratings.csv"), true);
            StringBuilder sb = new StringBuilder();
            if(filmList!=null && !filmList.isEmpty()){
                for(Film film: filmList){
                    if(film.getUserVotation()!=null){
                        sb.append(idUtente.toString());
                        sb.append(',');
                        sb.append(film.getId().toString());
                        sb.append(",");
                        sb.append(film.getUserVotation().toString());
                        sb.append(",");
                        sb.append(Calendar.getInstance().getTimeInMillis());
                        sb.append('\n');
                    }
                }
            }
            mFileWriter.write(sb.toString());
            mFileWriter.close();
        } catch (IOException e) {
            logger.error("Errore nel caricamento del file rating.csv");
        }
        return idUtente;
    }

/*
    @Autowired
    private HikariDataSource hikariDataSource;

    private static final int NEIGHBOR_HOOD_SIZE = 5;

    @Override
    public Recommender rielaborateQuestionOrderByOldQuestionnaire() throws TasteException {
        MySQLJDBCDataModel dataModel = new MySQLJDBCDataModel(hikariDataSource);
        UserSimilarity similarity = new PearsonCorrelationSimilarity(dataModel);

        UserNeighborhood neighborhood = new NearestNUserNeighborhood(
                NEIGHBOR_HOOD_SIZE, similarity, dataModel);

        org.apache.mahout.cf.taste.recommender.Recommender recommender = new GenericUserBasedRecommender(dataModel,
                neighborhood, similarity);

        return recommender;
    }
*/
}
