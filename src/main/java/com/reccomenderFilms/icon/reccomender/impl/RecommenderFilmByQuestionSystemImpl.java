package com.reccomenderFilms.icon.reccomender.impl;

import com.reccomenderFilms.icon.reccomender.RecommenderFilmByQuestionSystem;
import com.reccomenderFilms.icon.service.FilmService;
import com.reccomenderFilms.icon.structure.Film;
import com.reccomenderFilms.icon.structure.Questionario;
import com.reccomenderFilms.icon.util.GenreeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecommenderFilmByQuestionSystemImpl implements RecommenderFilmByQuestionSystem, InitializingBean {
    @Autowired
    private FilmService filmService;
    private List<Film> allFilm = new ArrayList<>();
    private Boolean isFirstQuery = Boolean.TRUE;
    private final static Logger logger = LoggerFactory.getLogger(RecommenderFilmByQuestionSystemImpl.class);

    @Override
    public List<Film> getFilmByQuestionnaire(List<Questionario> questionarioList) {
        Map<Double,List<Film>> filmConSimilaritàNonOrdinata = new HashMap<>();
        List<Film> consigliati = new ArrayList<>();
        //si collezzionano le caratteristice
        //si elabora un modello
        //si calcolo la similarità
        if(!isFirstQuery) {
            allFilm = filmService.getAllFilms();
        }
        isFirstQuery=Boolean.FALSE;
        for(Film film : allFilm){  //hai tutti i film con tutte le caratteriste
            Double similiraty = calculateFilmsSimilarity(film,questionarioList);
            List<Film> films = new ArrayList<>();
            if(filmConSimilaritàNonOrdinata.containsKey(similiraty)){
                films = filmConSimilaritàNonOrdinata.get(similiraty);
                films.add(film);
            }else{
                films.add(film);
                filmConSimilaritàNonOrdinata.put(similiraty,films);
            }
        }
        List<Double> keySet = (List<Double>) (List<?>) Arrays.asList(filmConSimilaritàNonOrdinata.keySet().toArray());
        Collections.sort(keySet);
        Collections.reverse(keySet);
        while(consigliati.size()!=28){
            for(int i =0 ; i< keySet.size() && consigliati.size()<28; i++){
                List<Film> filmFromKey = filmConSimilaritàNonOrdinata.get(keySet.get(i));
                for(int j=0; j<filmFromKey.size() && consigliati.size()<28; j++ ){
                    consigliati.add(filmFromKey.get(j));
                }
            }
        }
        return consigliati;
    }

    private Double calculateFilmsSimilarity(Film film, List<Questionario> questionarioList) {
        Double similarity = 0.0;
        int i = 1;
        if (film.getGenreeList() != null &&
                film.getGenreeList().stream().filter(genere -> genere.getDescription().equalsIgnoreCase(questionarioList.get(1).getAnswer().getAnswer().toUpperCase())).findFirst().isPresent()) {
            similarity += 1;
        }
        i++;
        if(questionarioList.get(i).getQuestion().getQuestion().contains("limite di eta'")) {
            if (film.getGenreeList() != null && film.getGenreeList().stream().filter(genere -> genere.getDescription().equalsIgnoreCase("horror")).findFirst().isPresent()) {
                if (film.getIsRestricted() != null && (film.getIsRestricted() && (questionarioList.get(i).getAnswer().getAnswer()).equalsIgnoreCase("1")) || (!film.getIsRestricted() && (questionarioList.get(i).getAnswer().getAnswer()).equalsIgnoreCase("0"))) {
                    similarity += 1;
                }
            }
            i++;
        }
        if (film.getWinOscar() != null && (film.getWinOscar() && questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("si")) || (!film.getWinOscar() && questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("no"))) {
            similarity += 1;
        }
        i++;
        if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("si")) {
            i++;
            if (film.getImdbRating() != null && film.getImdbRating().compareTo(new BigDecimal(questionarioList.get(i).getAnswer().getAnswer())) >= 0) {
                similarity += 1;
            } else if (film.getImdbRating() != null && (film.getImdbRating().compareTo(new BigDecimal(questionarioList.get(i).getAnswer().getAnswer()).subtract(BigDecimal.ONE)) >= 0) && (film.getImdbRating().compareTo(new BigDecimal(questionarioList.get(i).getAnswer().getAnswer())) < 0)) {
                similarity += 0.75;
            } else if (film.getImdbRating() != null && (film.getImdbRating().compareTo(new BigDecimal(questionarioList.get(i).getAnswer().getAnswer()).subtract(new BigDecimal(2))) >= 0) && ((film.getImdbRating().compareTo(new BigDecimal(questionarioList.get(i).getAnswer().getAnswer()).subtract(BigDecimal.ONE)) < 0))) {
                similarity += 0.15;
            }
        }
        i++;
        if (film.getIsFilmReflectRealLife() != null && (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("si") && film.getIsFilmReflectRealLife()) || (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("no") && !film.getIsFilmReflectRealLife())) {
            similarity += 1;
        }
        i++;
//        if(film.getReleased()!=null && ((getToday().compareTo(film.getReleased())>=0 &&  film.getReleased().compareTo(getOneWeekDate())>=0) && (film.getImdbRating().compareTo(BigDecimal.valueOf(6.0)))>=0) && questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("si") || ((getToday().compareTo(film.getReleased())>=0 &&  film.getReleased().compareTo(getOneWeekDate())<0) && ((film.getImdbRating().compareTo(BigDecimal.valueOf(6.0)))>=0) || (film.getImdbRating().compareTo(BigDecimal.valueOf(6.0)))<0) && questionarioList.get(7).getAnswer().getAnswer().equalsIgnoreCase("no")){
//            similarity+=1;
//
//        }
        i++;
        if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("meno di 5 anni")) {
            if (film.getReleased() != null && getToday().compareTo(film.getReleased()) >= 0 && film.getReleased().compareTo(getYearsDate(5)) >= 0)
                similarity += 1;
        } else if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("meno di 10 anni")) {
            if (film.getReleased() != null && getToday().compareTo(film.getReleased()) >= 0 && film.getReleased().compareTo(getYearsDate(10)) >= 0)
                similarity += 1;
        } else if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("meno di 15 anni")) {
            if (film.getReleased() != null && getToday().compareTo(film.getReleased()) >= 0 && film.getReleased().compareTo(getYearsDate(15)) >= 0)
                similarity += 1;
        } else if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("piu di 15 anni")) {
            if (film.getReleased() != null && getToday().compareTo(film.getReleased()) >= 0 && film.getReleased().compareTo(getYearsDate(15)) < 0)
                similarity += 1;
        }
        i++;
        if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("cortometraggio")) {
            if (film.getRuntime() != null && (film.getRuntime() < 90)) {
                similarity += 1;
            }
        } else if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("lungometraggio")) {
            if (film.getRuntime() != null && (film.getRuntime() > 90))
                similarity += 1;
        }
        i++;
        if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("italiano") && film.getCountryList().stream().filter(country -> country.getName().equalsIgnoreCase("italian")).findFirst().isPresent()) {
            similarity += 1;
        } else if (questionarioList.get(i).getAnswer().getAnswer().equalsIgnoreCase("internazionale") && film.getCountryList().stream().filter(country -> country.getName().equalsIgnoreCase("english")).findFirst().isPresent()){
            similarity += 1;
        }
        return similarity;
    }

    public Date getToday(){
        return Calendar.getInstance().getTime();
    }

    public Date getOneWeekDate(){
        Calendar cal =  Calendar.getInstance();
        cal.add(Calendar.MONTH,-2);
        return cal.getTime();
    }
    public Date getYearsDate(int year){
        Calendar cal =  Calendar.getInstance();
        cal.add(Calendar.YEAR,-year);
        return cal.getTime();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Carico tutti i film da db.......");
        this.allFilm = filmService.getAllFilms();
        logger.info("Caricati tutti i film da db. correttamente!");
    }
}


