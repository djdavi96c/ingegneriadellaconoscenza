package com.reccomenderFilms.icon.reccomender;


import com.reccomenderFilms.icon.structure.Film;
import com.reccomenderFilms.icon.structure.Questionario;

import java.util.List;

public interface RecommenderFilmByQuestionSystem{

    public List<Film> getFilmByQuestionnaire(List<Questionario> questionarioList);
}
