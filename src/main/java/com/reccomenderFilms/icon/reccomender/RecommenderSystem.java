package com.reccomenderFilms.icon.reccomender;

import com.reccomenderFilms.icon.structure.Film;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.recommender.Recommender;

import java.util.List;


public interface RecommenderSystem {
    Recommender rielaborateQuestionOrderByOldQuestionnaire() throws TasteException;
    Long insertFilmVotationToCsv(List<Film> filmList,Long idUtente);
}
