package com.reccomenderFilms.icon.controller;

import com.reccomenderFilms.icon.reccomender.RecommenderFilmByQuestionSystem;
import com.reccomenderFilms.icon.reccomender.RecommenderSystem;
import com.reccomenderFilms.icon.service.FilmService;
import com.reccomenderFilms.icon.service.QuestionService;
import com.reccomenderFilms.icon.service.QuestionnaireService;
import com.reccomenderFilms.icon.service.structure.IMBDInformation;
import com.reccomenderFilms.icon.structure.Answer;
import com.reccomenderFilms.icon.structure.Film;
import com.reccomenderFilms.icon.structure.Question;
import com.reccomenderFilms.icon.structure.Questionario;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Scope("view")
@ManagedBean
@Getter
@Setter
public class QuestionController {
    Logger logger = LoggerFactory.getLogger(QuestionController.class);

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private RecommenderFilmByQuestionSystem recommenderFilmByQuestionSystem;

    @Autowired
    private RecommenderSystem recommender;

    @Autowired
    private FilmService filmService;

    @Value("${openimbd.api.key}")
    private String openImbdApi;

    private List<Film> recommenderFilm = new ArrayList<>();

    private List<Question> questionList  = new ArrayList<>();

    private Question currentQuestion;

    private List<Questionario> questionarioCompilato;

    private Boolean isFilmMode = Boolean.FALSE;

    private Boolean isShowFilmMode = Boolean.FALSE;

    private Long idUtente;

    private RestTemplate restTemplate= new RestTemplateBuilder().build();

    @PostConstruct
    public void init(){
        currentQuestion = questionService.retrieveFirstQuestion();
        questionList.add(currentQuestion);
        questionList.addAll(questionService.getAllQuestion());
        questionarioCompilato = new ArrayList<>();
    }

    public void nextQuestion(Answer answer){
        if(answer.getNextQuestionId()!=null) {
            collectQuestionAndAnswer(currentQuestion,answer);
            Optional<Question> nextQuestionObject = questionList.stream().filter(question -> answer.getNextQuestionId().equals(question.getId())).findFirst();
            if (nextQuestionObject.isPresent()) {
                currentQuestion = nextQuestionObject.get();
            } else {
                logger.error("Impossibile trovare la domanda successiva per la domanda: {} ", currentQuestion);
            }
        }else{
            collectQuestionAndAnswer(currentQuestion,answer);
            //ho terminato le domande le salvo sul db
            saveQuestionnaire();
            retrieveRecommenderFilmByQuestion();
            if(recommenderFilm!=null && !recommenderFilm.isEmpty()){
                isFilmMode=Boolean.TRUE;
                getPosterFilms();
            }
        }
    }

    private void getPosterFilms() {
        for(Film film : recommenderFilm){
            if(film!=null && StringUtils.isNotBlank(film.getImbdId())) {
                IMBDInformation imbdInformation = restTemplate.getForObject("http://www.omdbapi.com/?apikey=".concat(openImbdApi).concat("+&i=").concat(film.getImbdId()), IMBDInformation.class);
                film.setLinkImage(StringUtils.isNotBlank(imbdInformation.getPoster()) && !imbdInformation.getPoster().equalsIgnoreCase("n/a") ? imbdInformation.getPoster() : "https://scontent-fco2-1.xx.fbcdn.net/v/t1.6435-9/49937862_285698845425496_6097625186788442112_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=Bec9MVwCP6MAX-wcBUM&_nc_ht=scontent-fco2-1.xx&oh=099f4f0819fe49274dc31bb8a526d178&oe=615B93EF");
            }
        }
    }

    public void collectQuestionAndAnswer(Question question, Answer answer){
        questionarioCompilato.add(new Questionario(question,answer));
   }

   public void saveQuestionnaire(){
        if(questionarioCompilato!=null && !questionarioCompilato.isEmpty()){
            questionnaireService.saveQuestionarioInformation(questionarioCompilato);
        }else{
            logger.error("Impossibile salvare questionario, non ci sono domande/risposte da salvare");
        }
   }

   public void retrieveRecommenderFilmByQuestion(){
       this.recommenderFilm=recommenderFilmByQuestionSystem.getFilmByQuestionnaire(questionarioCompilato);
   }

   public void elaborateFilmByVote(){
       idUtente = recommender.insertFilmVotationToCsv(recommenderFilm.stream().filter(p-> p.getUserVotation()!=null).collect(Collectors.toList()),idUtente);
       recommenderFilm = recommenderFilm.stream().filter(p->p.getUserVotation()==null).collect(Collectors.toList());
       try {
           Recommender recommenderSystem = recommender.rielaborateQuestionOrderByOldQuestionnaire();
           List<RecommendedItem> recommendedItems = recommenderSystem.recommend(idUtente,12);
           if(recommendedItems!=null && !recommendedItems.isEmpty()){
               recommendedItems.stream().forEach(rec-> recommenderFilm.add(filmService.getFilmById(rec.getItemID())));
               getPosterFilms();
           }
       } catch (TasteException e) {
           logger.error("Errore nell'elaborazione dei film consigliati");
       }
   }


}
