package com.reccomenderFilms.icon.util;

import com.reccomenderFilms.icon.ReccomenderFilmsApplication;
import com.reccomenderFilms.icon.mapper.CountryMapper;
import com.reccomenderFilms.icon.mapper.FilmMapper;
import com.reccomenderFilms.icon.mapper.GenreMapper;
import com.reccomenderFilms.icon.service.structure.IMBDInformation;
import com.reccomenderFilms.icon.structure.Country;
import com.reccomenderFilms.icon.structure.Film;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.function.DoubleUnaryOperator;

public class TaskRetrieveInformationIMBD implements Callable<String> {
    private static final Logger logger = LoggerFactory.getLogger(TaskRetrieveInformationIMBD.class);
    private RestTemplate restTemplate;
    private List<String> films;
    private FilmMapper filmMapper;
    private CountryMapper countryMapper;
    private GenreMapper genreMapper;
    private String openImbdApi;
    private String patternSearch;
    private final SimpleDateFormat fmt =  new SimpleDateFormat("dd MMM yyyy", Locale.US);

    public TaskRetrieveInformationIMBD(RestTemplate restTemplate, List<String> films, FilmMapper filmMapper, CountryMapper countryMapper,GenreMapper genreMapper,String openImbdApi,String patternSearch) {
        this.restTemplate = restTemplate;
        this.films = films;
        this.filmMapper=filmMapper;
        this.countryMapper=countryMapper;
        this.genreMapper=genreMapper;
        this.openImbdApi=openImbdApi;
        this.patternSearch=patternSearch;
    }

    public String call() throws Exception {

        for (String film: films) {
            Boolean awards=false, restricted=false,isFilmReflectRealLife= false;
            Double rating=null;
            Date uDate= null;
            Integer duration = null;
            try {
                IMBDInformation imbdInformation = restTemplate.getForObject("http://www.omdbapi.com/?apikey=".concat(openImbdApi).concat("&").concat(patternSearch).concat(patternSearch.equalsIgnoreCase("i")?"=tt":"").concat(film), IMBDInformation.class);
                if (imbdInformation.getRated() != null && imbdInformation.getRated().equals("R")) {
                    restricted = true;
                }
                if (imbdInformation.getAwards() != null && imbdInformation.getAwards().contains("Oscar")){
                    awards = true;
                }
                if(StringUtils.isNotBlank(imbdInformation.getGenre()) && (imbdInformation.getGenre().contains("Biography") || imbdInformation.getGenre().contains("Documentary"))){
                    isFilmReflectRealLife=true;
                }
                rating = StringUtils.isNotBlank(imbdInformation.getImdbRating()) && !imbdInformation.getImdbRating().contains("N/A") ? new Double(imbdInformation.getImdbRating()) : 0.0;
                uDate = imbdInformation.getReleased() != null && !imbdInformation.getReleased().contains("N/A") ? fmt.parse(imbdInformation.getReleased()) : null;
                java.sql.Date sDate = uDate != null ? convertUtilToSql(uDate) : null;
                duration = StringUtils.isNotBlank(imbdInformation.getRuntime()) && !imbdInformation.getRuntime().contains("N/A") ? Integer.valueOf(imbdInformation.getRuntime().substring(0, imbdInformation.getRuntime().indexOf(" "))) : 0;
                try {
                    Film film1 = new Film();
                    if (sDate != null) {
                        filmMapper.insertFilm(imbdInformation.getImdbID(),imbdInformation.getTitle().replace("'", ""), restricted, awards, rating, sDate, duration, isFilmReflectRealLife, film1);
                        if(StringUtils.isNotBlank(imbdInformation.getGenre())){
                            List<String> listaDiGeneri = Arrays.asList(imbdInformation.getGenre().split(","));
                            for(String genreItem : listaDiGeneri) {
                                Long idGenre= genreMapper.retrieveGenre(genreItem.replace(" ",""));
                                if(idGenre!=null && idGenre>0) {
                                    genreMapper.insertFilmGenreCorrelation(idGenre, film1.getId());
                                }
                            }
                        }
                    }
                    if(film1.getId()!=null) {
                        if (StringUtils.isNotBlank(imbdInformation.getLanguage())) {
                            List<String> countries = Arrays.asList(imbdInformation.getLanguage().split(","));
                            if (countries != null && !countries.isEmpty()) {
                                for (String country : countries) {
                                    Long idCountry = countryMapper.retrieveCountry(country.replace(" ", "").toUpperCase());
                                    Country countryElem = new Country();
                                    if (idCountry == null || idCountry < 0) {
                                        countryMapper.insertCountry(countryElem, country.replace(" ", "").toUpperCase());
                                    } else {
                                        countryElem.setId(idCountry);
                                    }
                                    countryMapper.insertCountryFilmCorrelation(film1.getId(), countryElem.getId());
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.info(e.toString());
                }
            }catch (Exception e){
                continue;
            }
        }

        logger.info("Ho terminato il caricamento delle informazioni aggiuntive "+films.size());

        return films.size()+"";
    }

    private static java.sql.Date convertUtilToSql(java.util.Date uDate) {
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }
}
