package com.reccomenderFilms.icon.util;

public enum GenreeConverter {

    ANIMAZIONE("Animation"),
    HORROR("Horror"),
    AZIONE("Action"),
    FAMIGLIA("Children"),
    COMMEDIE("Comedy"),
    CRIME("Crime"),
    DOCUMENTARI("Documentary"),
    DRAMMI("Drama"),
    FANTASCIENZA("Sci-fi");

    public final String name;

    GenreeConverter(String i) {
        name=i;
    }
}
