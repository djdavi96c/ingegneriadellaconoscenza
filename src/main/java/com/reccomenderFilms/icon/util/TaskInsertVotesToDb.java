package com.reccomenderFilms.icon.util;

import com.reccomenderFilms.icon.mapper.RatingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.Callable;

public class TaskInsertVotesToDb implements Callable<String> {
    private static final Logger logger = LoggerFactory.getLogger(TaskInsertVotesToDb.class);
    private List<String> votes;
    private RatingMapper ratingMapper;


    public TaskInsertVotesToDb( List<String> votes, RatingMapper ratingMapper) {
        this.votes = votes;
        this.ratingMapper=ratingMapper;
    }

    public String call() throws Exception {

        for (String vote: votes) {
            String[] film = vote.split(",");
            Integer user = Integer.valueOf(film[0]);
            Integer movie = Integer.valueOf(film[1]);
            BigDecimal rating = BigDecimal.valueOf(Double.valueOf(film[2]));
            ratingMapper.insertRating(user,movie,rating);
        }

        logger.info("Ho terminato il caricamento delle informazioni aggiuntive "+votes.size());

        return votes.size()+"";
    }
}
