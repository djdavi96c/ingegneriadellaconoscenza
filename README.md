# IngegneriaDellaConoscenza

Progetto di ingegneria della conoscenza su sistemi di apprendimento e raccomandazione


Requisiti necessari:
#Maven 3.5.3+ 
#Java 1.8+
#Mysql
#Redis

Compilazione:
Lanciare il comando "mvn clean install"

Far partire l'applicativo:
Avviare prima redis server e poi l'applicativo tramite il comando
mvn spring-boot:run (N.B deve essere installato mysql e bisogna cambiare le impostazioni di collegamento tramite application.properties)
(N.B 2 mvn spring-boot:run in realtà builda in automatico)
